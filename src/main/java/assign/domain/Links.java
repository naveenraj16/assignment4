package assign.domain;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "project")
@XmlType(propOrder = {"name","links"})
public class Links {

private String name = null;
private List<String> links = null;

public String getName() {
	return name;
}

@XmlElement(name = "name")
public void setName(String name) {
	this.name = name;
}

public List<String> getLinks() {
	return links;
}

@XmlElement(name = "link")
public void setLinks(List<String> links) {
	this.links = links;
}
}