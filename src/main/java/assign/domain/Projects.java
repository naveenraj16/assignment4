package assign.domain;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "projects")
public class Projects {

private List<String> projectList = null;

public List<String> getProjects() {
	return projectList;
}

@XmlElement(name = "project")
public void setProjects(List<String> projects) {
	this.projectList = projects;
}
}