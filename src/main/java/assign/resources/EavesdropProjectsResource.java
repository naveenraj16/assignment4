package assign.resources;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import assign.domain.Links;
import assign.domain.Projects;
import assign.services.LinksServiceImpl;
import assign.services.ProjectNamesServiceImpl;

@Path("/myeavesdrop")
public class EavesdropProjectsResource {
	
	
public EavesdropProjectsResource() {
}



@GET
@Path("/helloworld")
@Produces("text/html")
public String helloWorld() {
return "Hello world";
}

@GET
@Path("/projects/{pname}/irclogs")
@Produces("application/xml")
public StreamingOutput getAllLinksInIrclogs(@PathParam("pname") String projectName) throws Exception {
	final Links links = new Links();
	LinksServiceImpl lsi = new LinksServiceImpl();
	ArrayList<String> a = lsi.getLinks(projectName);
	links.setName(projectName);
	links.setLinks(a);
	return new StreamingOutput() {
		public void write(OutputStream outputStream) throws IOException, WebApplicationException {
		outputLinks(outputStream, links);
		}
		};
}


@GET
@Path("/projects")
@Produces("application/xml")
public StreamingOutput getAllProjects() throws Exception {
	final Projects projects = new Projects();
	ProjectNamesServiceImpl pns = new ProjectNamesServiceImpl();
	ArrayList<String> a = pns.getProjectNames();
	projects.setProjects(a);

	return new StreamingOutput() {
		public void write(OutputStream outputStream) throws IOException, WebApplicationException {
			outputProjects(outputStream, projects);
		}
	};
}

protected void outputLinks(OutputStream os, Links links) throws IOException {
	try {
		JAXBContext jaxbContext = JAXBContext.newInstance(Links.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		jaxbMarshaller.marshal(links, os);
} catch (JAXBException jaxb) {
	jaxb.printStackTrace();
	throw new WebApplicationException();
}
	
}
protected void outputProjects(OutputStream os, Projects projects) throws IOException {
	try {
		JAXBContext jaxbContext = JAXBContext.newInstance(Projects.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		jaxbMarshaller.marshal(projects, os);
} catch (JAXBException jaxb) {
	jaxb.printStackTrace();
	throw new WebApplicationException();
}
}

}