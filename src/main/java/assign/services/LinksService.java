package assign.services;

import java.util.ArrayList;

public interface LinksService {
	
	public ArrayList<String> getLinks(String project);

}
