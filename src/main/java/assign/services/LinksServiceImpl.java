package assign.services;

import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class LinksServiceImpl {
	
	final static String BASE_URL = "http://eavesdrop.openstack.org/irclogs/";

	public ArrayList<String> getLinks(String project){
		ArrayList<String> result = new ArrayList<String>();
		Document doc;
	        try {

	        		project = URLEncoder.encode(project,"UTF-8");
	                // need http protocol
	                doc = Jsoup.connect(BASE_URL+project+"/").get();
	                
	                // get all links for a project in irclogs
	                Elements links = doc.select("a[href]");
	                for(int i = 5; i < links.size(); i++){
	                        Element ele = links.get(i);
	                        String link = ele.attr("href");
	                        link = URLDecoder.decode(link,"UTF-8");
	                        result.add(BASE_URL+project+"/"+link);
	                }

	             

	        } catch (IOException e) {
	                //e.printStackTrace();
	        		result.add("Invalid Project");
	                return result;
	        }
		return result;
	}
	
}
