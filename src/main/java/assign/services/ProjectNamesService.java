package assign.services;

import java.util.ArrayList;

public interface ProjectNamesService {

	public ArrayList<String> getProjectNames();
	
}
