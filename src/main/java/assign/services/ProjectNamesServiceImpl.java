package assign.services;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ProjectNamesServiceImpl implements ProjectNamesService{
	
	final static String BASE_URL1 = "http://eavesdrop.openstack.org/meetings/";
	final static String BASE_URL2 = "http://eavesdrop.openstack.org/irclogs/";
	
	public ArrayList<String> getProjectNames(){
		 ArrayList<String> result = new ArrayList<String>();
		    Document doc1;
		    Document doc2;
		        try {

		                // need http protocol
		                doc1 = Jsoup.connect(BASE_URL1).get();


		                // get all projects in meetings
		                Elements projects1 = doc1.select("a[href]");
		                for(int i = 5; i < projects1.size(); i++){
		                        Element ele = projects1.get(i);
		                        String name = ele.text();
		                        name = name.substring(0,name.length()-1);
		                        result.add(name);
		                }
		                
		                doc2 = Jsoup.connect(BASE_URL2).get();

		                // get all projects in irclogs
		                Elements projects2 = doc2.select("a[href]");
		                for(int i = 5; i < projects2.size(); i++){
		                        Element ele = projects2.get(i);
		                        String name = ele.text();
		                        name = name.substring(0,name.length()-1);
		                        result.add(name);
		                }

		        } catch (IOException e) {
		                //e.printStackTrace();
		                return result;
		        }
		        return result;
	}

}
