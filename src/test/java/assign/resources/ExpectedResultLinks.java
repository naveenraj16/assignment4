package assign.resources;

import java.util.ArrayList;

public class ExpectedResultLinks {
    public static ArrayList<String> expectedResultLinks() {
        ArrayList<String> expectedResult = new ArrayList<String>();

expectedResult.add("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-03-05.log");
expectedResult.add("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-03-06.log");
expectedResult.add("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-03-07.log");
expectedResult.add("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-03-08.log");
expectedResult.add("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-03-09.log");
expectedResult.add("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-03-10.log");
expectedResult.add("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-03-11.log");
expectedResult.add("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-03-12.log");
expectedResult.add("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-03-13.log");
expectedResult.add("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-03-14.log");
expectedResult.add("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-03-15.log");
expectedResult.add("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-03-16.log");
expectedResult.add("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-03-17.log");
expectedResult.add("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-03-18.log");
expectedResult.add("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-03-19.log");
expectedResult.add("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-03-20.log");
expectedResult.add("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-03-21.log");
expectedResult.add("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-03-22.log");
expectedResult.add("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-03-23.log");
expectedResult.add("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-03-24.log");
expectedResult.add("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-03-25.log");
expectedResult.add("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-03-26.log");
expectedResult.add("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-03-27.log");
expectedResult.add("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-03-28.log");
expectedResult.add("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-03-29.log");
expectedResult.add("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-03-30.log");
expectedResult.add("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-03-31.log");
expectedResult.add("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-04-01.log");
expectedResult.add("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-04-02.log");
expectedResult.add("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-04-03.log");
expectedResult.add("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-04-04.log");

        return expectedResult;
    }
}
