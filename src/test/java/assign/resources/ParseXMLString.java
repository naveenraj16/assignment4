package assign.resources;

import javax.xml.parsers.*;

import org.xml.sax.InputSource;
import org.w3c.dom.*;

import java.io.*;
import java.util.ArrayList;

public class ParseXMLString {

      public static ArrayList<String> actualResultProjects(String xmlRecords) {
          ArrayList<String> result = new ArrayList<String>();
          try {
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(xmlRecords));

                Document doc = db.parse(is);
                NodeList nodes = doc.getElementsByTagName("project");

                for (int i = 0; i < nodes.getLength(); i++) {
                   Element element = (Element) nodes.item(i);
                   result.add(element.getTextContent());
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
          return result;
      }

      public static ArrayList<String> actualResultLinks(String xmlRecords) {
          ArrayList<String> result = new ArrayList<String>();
          try {
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(xmlRecords));

                Document doc = db.parse(is);
                NodeList nodes = doc.getElementsByTagName("link");

                for (int i = 0; i < nodes.getLength(); i++) {
                   Element element = (Element) nodes.item(i);
                   result.add(element.getTextContent());
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
          return result;
      }

}