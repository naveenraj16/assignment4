package assign.resources;

import java.util.ArrayList;

import javax.ws.rs.core.Response;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestResources {

    @Before
    public void setUp() {

    }

    @Test
    public void testResource1() {
        ArrayList<String> expectedProjects = ExpectedResultProjects.expectedResultProjects();
        int expectedSize = expectedProjects.size();

        ResteasyClient client = new ResteasyClientBuilder().build();
        ResteasyWebTarget target = client.target("http://localhost:8080/assignment4/myeavesdrop/projects");
        Response response = target.request().get();
        String xmlRecords = response.readEntity(String.class);
        response.close();
        ArrayList<String> actualProjects = ParseXMLString.actualResultProjects(xmlRecords);
        int obtainedSize = actualProjects.size();

        assertEquals(expectedProjects,actualProjects);
       
    }
    
    @Test
    public void testResource2() {
        ArrayList<String> expectedLinks = ExpectedResultLinks.expectedResultLinks();

        ResteasyClient client = new ResteasyClientBuilder().build();
        ResteasyWebTarget target = client.target("http://localhost:8080/assignment4/myeavesdrop/projects/%23openstack-api/irclogs");
        Response response = target.request().get();
        String xmlRecords = response.readEntity(String.class);
        response.close();
        ArrayList<String> actualLinks = ParseXMLString.actualResultLinks(xmlRecords);

        assertEquals(expectedLinks,actualLinks);
       
    }
    
    @Test
    public void testResource3() {
        ArrayList<String> expectedLinks = new ArrayList<String>();
        expectedLinks.add("Invalid Project");

        ResteasyClient client = new ResteasyClientBuilder().build();
        ResteasyWebTarget target = client.target("http://localhost:8080/assignment4/myeavesdrop/projects/non-existant-project/irclogs");
        Response response = target.request().get();
        String xmlRecords = response.readEntity(String.class);
        response.close();
        ArrayList<String> actualLinks = ParseXMLString.actualResultLinks(xmlRecords);

        assertEquals(expectedLinks,actualLinks);
       
    }
    
}